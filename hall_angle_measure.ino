#include <Arduino.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <Adafruit_DotStar.h>
#include <SPI.h> // 필요한 경우 SPI 라이브러리 포함
#include <cmath> // fmod 함수 사용을 위한 헤더 추가

#define HALL_SENSOR_PIN 36
volatile bool magnetDetected = false;
unsigned long lastDetectionTime = 0;
unsigned long rotationTime = 0;
float currentAngle = 0.0;
float targetAngle = 180.0;
float angleMargin = 2.0;
unsigned int nCurrentAngle = 0;
const int hallSensorPin = HALL_SENSOR_PIN; // 홀 센서 연결 핀
const int threshold = 1600; // 자석 감지를 위한 ADC 임계값

// DotStar LED 설정
#define DOTSTAR_NUM 40 // LED 수
#define PIN_DOTSTAR_DATA 23 // 데이터 핀
#define PIN_DOTSTAR_CLK 18 // 클럭 핀
Adafruit_DotStar strip(DOTSTAR_NUM, PIN_DOTSTAR_DATA, PIN_DOTSTAR_CLK, DOTSTAR_BGR);

int sensorValue;

void combinedTask(void *parameter) {
  while (true) {
    // 홀 센서 값 읽기
//    int sensorValue = analogRead(hallSensorPin);
    sensorValue = analogRead(hallSensorPin);
    if (sensorValue < threshold && !magnetDetected) {
      magnetDetected = true;
      unsigned long currentTime = millis();
      rotationTime = currentTime - lastDetectionTime;
      lastDetectionTime = currentTime;
      currentAngle = 0.0;
    } else if (sensorValue >= threshold) {
      magnetDetected = false;
    }

    // 각도 계산
    if (rotationTime > 0) {
      unsigned long currentTime = millis();
      unsigned long timeSinceLastDetection = currentTime - lastDetectionTime;
      currentAngle = (timeSinceLastDetection / (float)rotationTime) * 360.0;
      if (currentAngle >= 360.0) {
        currentAngle -= 360.0;
      }


      // LED 제어
      if (currentAngle >= (targetAngle-angleMargin) && currentAngle <= (targetAngle+angleMargin)) {
      //if (fmod(currentAngle, 90.0) == 0.0) {
        strip.setPixelColor(0, strip.Color(255, 0, 0)); // 빨간색으로 설정
        strip.show();
      } else {
        strip.clear(); // LED 끔
        strip.show();
      }
    }
    vTaskDelay(5 / portTICK_PERIOD_MS);
    
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(hallSensorPin, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(hallSensorPin), detectMagnet, RISING);
  strip.begin(); // DotStar LED 초기화
  
  strip.setPixelColor(0, strip.Color(255, 255, 0)); // 빨간색으로 설정
  strip.setPixelColor(1, strip.Color(255, 255, 0)); // 빨간색으로 설정
  strip.show();  // 모든 LED를 꺼서 시작
  
  xTaskCreate(combinedTask, "Combined Task", 2000, NULL, 1, NULL);
}

void loop() {
  // 메인 루프는 비워둠
  //sensorValue = analogRead(hallSensorPin);
  //delay(10);
  Serial.printf("fdlkfjlfjdljslfjslfjslfjaljfaks\n");
}